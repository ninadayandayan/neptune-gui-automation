$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features/TC146_ValidateMarketDepth.feature");
formatter.feature({
  "name": "Advanced Search: Search for specific Security ID and validate Market Depth load",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Advanced Search: Search for specific Security ID and validate Market Depth load",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.step({
  "name": "enter single full \u003cvalue\u003e search on Security ID field",
  "keyword": "And "
});
formatter.step({
  "name": "User select indication and validate market depth load",
  "keyword": "And "
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "value"
      ]
    },
    {
      "cells": [
        "DE000A0T7J03"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Advanced Search: Search for specific Security ID and validate Market Depth load",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter single full DE000A0T7J03 search on Security ID field",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC149458_Steps.enterSingleFullSearchOnSecurityIDField(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select indication and validate market depth load",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC146_Steps.user_select_indication_and_validate_market_depth_load()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/TC149458_PerformAS_usingSecurityID.feature");
formatter.feature({
  "name": "Advanced Search: Enter a search using Security ID",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Enter a search with partial Security ID",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.step({
  "name": "enter single partial \u003cvalue\u003e search on Security ID field",
  "keyword": "And "
});
formatter.step({
  "name": "verify if search parameter \u003cvalue\u003e is displayed in Dashboard - Partial Security ID",
  "keyword": "Then "
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as \u003cquery\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "value",
        "query"
      ]
    },
    {
      "cells": [
        "DE",
        "DE"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Enter a search with partial Security ID",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter single partial DE search on Security ID field",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "verify if search parameter DE is displayed in Dashboard - Partial Security ID",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as DE",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC149458_Steps.verifyIfSearchParameterIsDisplayedInRecentSearchAs(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/TC211_PerformAS_usingSingleSeniority.feature");
formatter.feature({
  "name": "Advanced Search: Perform search using single Seniority",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Perform search using single Seniority",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.step({
  "name": "user selects single seniority on Seniority drop down list",
  "keyword": "And "
});
formatter.step({
  "name": "verify if search parameter \u003cvalue\u003e is displayed in Dashboard - Seniority",
  "keyword": "Then "
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as \u003cquery\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "value",
        "query"
      ]
    },
    {
      "cells": [
        "Senior",
        "Senior"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Perform search using single Seniority",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user selects single seniority on Seniority drop down list",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC211_Steps.user_selects_single_seniority_on_Seniority_drop_down_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify if search parameter Senior is displayed in Dashboard - Seniority",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC211_Steps.verify_if_search_parameter_is_displayed_in_Dashboard_Seniority(java.lang.String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Data not found!\r\n\tat org.junit.Assert.fail(Assert.java:89)\r\n\tat pageObjects.MainPage.verifyGrid_Seniority(MainPage.java:544)\r\n\tat Steps.TC211_Steps.verify_if_search_parameter_is_displayed_in_Dashboard_Seniority(TC211_Steps.java:30)\r\n\tat ✽.verify if search parameter Senior is displayed in Dashboard - Seniority(file:///C:/Users/NinaKarlaDayandayan/IdeaProjects/neptune-gui-automation/NeptuneGUI_SeleniumBDDFramework/src/test/java/Features/TC211_PerformAS_usingSingleSeniority.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as Senior",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC149458_Steps.verifyIfSearchParameterIsDisplayedInRecentSearchAs(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/TC214_PerformAS_usingSingleAssetClass.feature");
formatter.feature({
  "name": "Advanced Search: Perform search using single Asset Class",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Perform search using single Asset Class",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.step({
  "name": "user selects single asset class on Asset Class drop down list",
  "keyword": "And "
});
formatter.step({
  "name": "verify if search parameter \u003cvalue\u003e is displayed in Dashboard - Asset Class",
  "keyword": "Then "
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as \u003cquery\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "value",
        "query"
      ]
    },
    {
      "cells": [
        "Financial",
        "Financial"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Perform search using single Asset Class",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "When "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user selects single asset class on Asset Class drop down list",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC214_Steps.user_selects_single_asset_class_on_Asset_Class_drop_down_list()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify if search parameter Financial is displayed in Dashboard - Asset Class",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC214_Steps.verify_if_search_parameter_is_displayed_in_Dashboard_Asset_Class(java.lang.String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Data not found!\r\n\tat org.junit.Assert.fail(Assert.java:89)\r\n\tat pageObjects.MainPage.verifyGrid_AssetClass(MainPage.java:530)\r\n\tat Steps.TC214_Steps.verify_if_search_parameter_is_displayed_in_Dashboard_Asset_Class(TC214_Steps.java:28)\r\n\tat ✽.verify if search parameter Financial is displayed in Dashboard - Asset Class(file:///C:/Users/NinaKarlaDayandayan/IdeaProjects/neptune-gui-automation/NeptuneGUI_SeleniumBDDFramework/src/test/java/Features/TC214_PerformAS_usingSingleAssetClass.feature:7)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "verify if search parameter is displayed in Recent Search as Financial",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC149458_Steps.verifyIfSearchParameterIsDisplayedInRecentSearchAs(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/TC233_PerformAS_Fit_To_Screen.feature");
formatter.feature({
  "name": "User clicks fit to screen button",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User clicks fit to screen button",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press enter from keyboard",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC84_Steps.user_press_enter_from_keyboard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on fit to screen button",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC233_Steps.user_clicks_fit_to_screen_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/Features/TC84_Enter_To_Trigger_Search.feature");
formatter.feature({
  "name": "Press enter from keyboard to trigger search upon login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Press enter from keyboard to trigger search upon login",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "Given "
});
formatter.step({
  "name": "User should see the dashboard - \"\u003ctitle\u003e\" page",
  "keyword": "Then "
});
formatter.step({
  "name": "User press enter from keyboard",
  "keyword": "And "
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "title"
      ]
    },
    {
      "cells": [
        "Neptune"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Press enter from keyboard to trigger search upon login",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SanityTest1"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Advanced Search tab",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps.TC149458_Steps.user_clicks_on_Advanced_Search_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should see the dashboard - \"Neptune\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC01_Steps.user_should_see_the_dashboard_page(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press enter from keyboard",
  "keyword": "And "
});
formatter.match({
  "location": "Steps.TC84_Steps.user_press_enter_from_keyboard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sign out successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.TC149458_Steps.userSignOutSuccessfully()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});