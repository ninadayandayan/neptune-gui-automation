Feature: User clicks expand button

  @SanityTest
  Scenario: User clicks expand button
    Given User clicks on Advanced Search tab
    And User press enter from keyboard
    And User clicks on expand button
    Then User sign out successfully
