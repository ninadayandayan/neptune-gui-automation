Feature: Advanced Search: Enter a search using Security ID

  @SanityTest
  Scenario Outline:Enter a search with specific Security ID
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        |query      |
      | DE000A0T7J03|DE000A0T7J03|

  @SanityTest
  Scenario Outline: 2nd Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

     Examples:
       |value          |query            |
       |  BE6282459609 |  BE6282459609   |


  @SanityTest
  Scenario Outline: 3rd Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  DE000A11QR65 |  DE000A11QR65   |


  @SanityTest
  Scenario Outline:4th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  DE000A11QSB8 |  DE000A11QSB8   |

  @SanityTest
  Scenario Outline: 5th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  DE000A11QSB8 |  DE000A11QSB8   |

  @SanityTest
  Scenario Outline: 6th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  NL0010364139 |  NL0010364139   |

  @SanityTest
  Scenario Outline: 7th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  FR0123283717 |  FR0123283717   |

  @SanityTest
  Scenario Outline: 8th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  IT0005004426 |  IT0005004426   |

  @SanityTest
  Scenario Outline: 9th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  IT0004890882 |  IT0004890882   |

  @SanityTest
  Scenario Outline: 10th Full ISIN Search
    When User clicks on Advanced Search tab
    And enter single full <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      |value          |query            |
      |  DE0001135317 |  DE0001135317   |

  @onhold
  Scenario Outline:Enter a search with partial Security ID
    When User clicks on Advanced Search tab
    And enter single partial <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Partial Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        |query       |
      | DE           |DE          |