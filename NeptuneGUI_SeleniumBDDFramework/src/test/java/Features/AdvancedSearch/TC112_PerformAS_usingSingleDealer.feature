Feature: Advanced Search: Perform search using single dealer

  @onhold
  Scenario Outline:Perform search using single dealer
    When User clicks on Advanced Search tab
    And enter selects single dealer  on dealer drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Dealer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                             |  query              |
      |  Etrading Software Bank A         |  [All Securities]   |







