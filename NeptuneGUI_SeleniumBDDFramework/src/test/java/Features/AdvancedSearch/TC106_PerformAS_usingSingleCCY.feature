Feature: Advanced Search: Perform search using single currency

  @SanityTest
  Scenario Outline:Perform search using single currency
    When User clicks on Advanced Search tab
    And enter selects single ccy  on CCY drop down list
    Then verify if search parameter <value> is displayed in Dashboard -  CCY
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        |  query |
      |  EUR         |  EUR   |







