Feature: Advanced Search: Enter a search for Entity Name

  @SanityTest
  Scenario Outline:Enter a search with FULL Entity Name
    When User clicks on Advanced Search tab
    And enter parameter <value> search on Entity Name field
    Then verify if search parameter <value> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value                      | query                      |
      | Deutsche Bahn Finance B.V. | Deutsche Bahn Finance B.V. |


  @SanityTest
  Scenario Outline:Enter a search with partial Entity Name
    When User clicks on Advanced Search tab
    And enter parameter <value> search on Entity Name field
    Then verify if search parameter <value> is displayed in Dashboard - Entity Name
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value    | query    |
      | Deutsche | Deutsche |







