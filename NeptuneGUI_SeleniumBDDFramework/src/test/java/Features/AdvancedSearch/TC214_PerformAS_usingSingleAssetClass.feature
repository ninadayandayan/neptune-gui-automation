Feature: Advanced Search: Perform search using single Asset Class

  @SanityTest
  Scenario Outline:Perform search using single Asset Class
    When User clicks on Advanced Search tab
    And user selects single asset class on Asset Class drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Asset Class
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value     | query     |
      | Financial | Financial |









