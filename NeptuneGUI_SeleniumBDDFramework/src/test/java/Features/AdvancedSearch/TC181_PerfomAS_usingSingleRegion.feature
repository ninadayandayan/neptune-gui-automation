Feature: Advanced Search: Perform search using single Region

  @SanityTest
  Scenario Outline:Perform search using single region
    When User clicks on Advanced Search tab
    And user selects single region  on region drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Region
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query |
      | Asia  | Asia  |









