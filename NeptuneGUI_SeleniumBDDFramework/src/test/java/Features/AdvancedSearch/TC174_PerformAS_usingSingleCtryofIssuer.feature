Feature: Advanced Search: Perform search using single Country of Issuer

  @SanityTest
  Scenario Outline:Perform search using single Country of Issuer
    When User clicks on Advanced Search tab
    And user selects single Country of Issuer <country_code> on Ctry of Issuer drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Ctry of Issuer
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | country_code | value              | query                  |
      | USA          | United States      | United States (Issuer) |











