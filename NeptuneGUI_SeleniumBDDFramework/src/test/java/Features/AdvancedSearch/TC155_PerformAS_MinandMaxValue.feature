Feature: Advanced Search: Enter a search with min and max value
  @onhold
  Scenario Outline: Enter a search with min value
    When User clicks on Advanced Search tab
    And enter min  <value> search on Min field
    Then verify if search parameter <value> is displayed in Dashboard - MIN
    #Then verify if search parameter is displayed in Recent Search - MIN as <query>

    Examples:
      | value     | query         |
      | 2.50      | Yield 2.50    |









