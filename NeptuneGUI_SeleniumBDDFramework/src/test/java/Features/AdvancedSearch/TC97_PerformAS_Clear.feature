Feature: Clear search results from Advanced Search

  @SanityTest
  Scenario: Press enter from keyboard to trigger search upon login
    Given User clicks on Advanced Search tab
    And User press enter from keyboard
    And User press clear to clear search results
    Then User sign out successfully
