Feature: Advanced Search: Perform search using single Country of Risk

  @SanityTest
  Scenario Outline:Perform search using single Country of Risk
    When User clicks on Advanced Search tab
    And user selects single Country of Risk <country_code> on Ctry of Risk drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Ctry of Risk
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | country_code | value              | query                  |
      | GBR          | United Kingdom     | United Kingdom (Risk)  |

