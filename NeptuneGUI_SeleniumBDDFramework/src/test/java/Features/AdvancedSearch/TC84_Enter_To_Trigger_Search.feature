Feature: Press enter from keyboard to trigger search upon login

  @SanityTest
  Scenario Outline: Press enter from keyboard to trigger search upon login
    Given User clicks on Advanced Search tab
    Then User should see the dashboard - "<title>" page
    And User press enter from keyboard
    Then User sign out successfully
    Examples:
      | title |
      |Neptune|