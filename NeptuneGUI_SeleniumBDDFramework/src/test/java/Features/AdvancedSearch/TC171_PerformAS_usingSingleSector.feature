Feature: Advanced Search: Perform search using single sector

  @SanityTest
  Scenario Outline:Perform search using single sector
    When User clicks on Advanced Search tab
    And user selects single sector  on sector drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Sector
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query |
      | Banks | Banks |









