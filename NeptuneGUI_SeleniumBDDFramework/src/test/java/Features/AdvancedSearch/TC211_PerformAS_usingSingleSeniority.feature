Feature: Advanced Search: Perform search using single Seniority

  @SanityTest
  Scenario Outline:Perform search using single Seniority
    When User clicks on Advanced Search tab
    And user selects single seniority on Seniority drop down list
    Then verify if search parameter <value> is displayed in Dashboard - Seniority
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value  | query  |
      | Senior | Senior |









