Feature: Perform Advanced Search using Side

  @onhold
  Scenario Outline: Perform Advanced Search using Side
    When User clicks on Advanced Search tab
    And User select SIDE <value> from SIDE drop down list
    Then verify if side <value> is displayed in Dashboard
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully
    Examples:
      | value | query |
    |BID    |  BID     |