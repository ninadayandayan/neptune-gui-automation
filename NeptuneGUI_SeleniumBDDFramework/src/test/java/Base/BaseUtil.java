package Base;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

public class BaseUtil {

        public  static WebDriver driver;
        public  static WebDriverWait wait;
        public void waitforPageLoaded() {
                ExpectedCondition<Boolean> expectation = new
                        ExpectedCondition<Boolean>() {
                                public Boolean apply(WebDriver driver) {
                                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                                }
                        };
                try {
                        Thread.sleep(3000);
                        WebDriverWait wait = new WebDriverWait(driver, 30);
                        wait.until(expectation);
                } catch (Throwable error) {
                        Assert.fail("Timeout waiting for Page Load Request to complete.");
                }

        }

}


