package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    Actions action = new Actions(driver);


    //Elements:
    @FindBy(xpath = "//input[@id='sign-in-company']")
    @CacheLookup
    public WebElement txtCompany;

    @FindBy(xpath = "//input[@id='sign-in-username']")
    @CacheLookup
    public WebElement txtUsername;

    @FindBy(xpath = "//input[@id='sign-in-password']")
    @CacheLookup
    public WebElement txtPassword;

    @FindBy(xpath = "//button[text()='Sign in']")
    @CacheLookup
    public WebElement btnSignIn;

    @FindBy(xpath = "//li[@class='dropdown dropdown-usermenu']/a")
    @CacheLookup
    public WebElement btnMainMenu;

    @FindBy(xpath = "//li/a[@id='logout-link']")
    @CacheLookup
    WebElement btnSignOut;



    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver, this);
    }

    /*public LoginPage(WebDriver driver) {
        this.driver = driver;
        //this.wait = new WebDriverWait(driver,30);
        Initialize elements
        PageFactory.initElements(driver, this);

    }*/

    public void setCompanyID(String companyID) {
        //wait.until(ExpectedConditions.elementToBeClickable(txtCompany));
        //js.executeScript("document.getElementById('sign-in-company').value='DEV_BUY_1';");
        txtCompany.sendKeys(companyID);
        //System.out.println("Entered Company ID: " + companyID);
    }

    public void setUsername(String username) {
        //wait.until(ExpectedConditions.elementToBeClickable(txtUsername));
        txtUsername.clear();
        txtUsername.sendKeys(username);
        //System.out.println("Entered Username is:" + username);
    }

    public void setPassword(String password) throws AWTException {
        //wait.until(ExpectedConditions.elementToBeClickable(txtPassword));
        txtPassword.clear();
        txtPassword.sendKeys(password);
        //press enter from keyboard
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);


        //System.out.println("Entered password");
    }

    public void clickSignIn()  {
        wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnSignIn);
        //System.out.println("Triggered Sign in button");
    }

    public void selectFromMainMenu() throws InterruptedException {
        //wait.until(ExpectedConditions.elementToBeClickable(btnMainMenu));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnMainMenu);
        //btnMainMenu.click();
        action.doubleClick(btnMainMenu);
        Thread.sleep(6000);
        System.out.println("Triggered Main Menu");
    }

    public void clickLogout() {
        wait.until(ExpectedConditions.elementToBeClickable(btnSignOut));

        try {
            if(btnSignOut.isDisplayed()){
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnSignOut);
                action.doubleClick(btnSignOut);
                //btnSignOut.click();
                System.out.println("Triggered sign out button");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void verifyTitle(String title) {

        String actualTitle = driver.getTitle();

        if (title.equals(actualTitle)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Dashboard Page is successfully displayed" + "\n"+ "Title displayed is: " + actualTitle);
        } else {
            Assert.fail("Page did not load!");
            System.out.println("Page did not load!");
        }

    }
}
