package pageObjects;

import Base.BaseUtil;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AdvancedSearch extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input[@placeholder='Security ID']")
    @CacheLookup
    WebElement securityIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[2]/input[@placeholder='Security Name']")
    @CacheLookup
    WebElement securityNameField;

    @FindBy(xpath = "//button[@class='btn btn-primary search-form-performSearch']")
    @CacheLookup
    WebElement searchBtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[6]/div/div[2]/input[@placeholder='Entity Name']")
    @CacheLookup
    WebElement entityNameField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[2]/div/div/div/input[@name='yield_min']")
    @CacheLookup
    WebElement minIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[2]/div/div/div/input[@name='yield_max']")
    @CacheLookup
    WebElement maxIDField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/div/a[@class='chosen-single']")
    @CacheLookup
    WebElement sideDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select/option[1]")
    @CacheLookup
    WebElement BID;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select/option[2]")
    @CacheLookup
    WebElement ASK;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/button[@class='multiselect dropdown-toggle btn btn-default ccy-multiselect']")
    ////*[@id="dashboard-tab"]/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/button
    @CacheLookup
    WebElement ccyDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[1]/a/label/input[1]")
    @CacheLookup
    WebElement ccy_AllChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[3]/a/label/input[@value='EUR_top']")
    @CacheLookup
    WebElement ccy_EURChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[3]/a/label/input[@value='USD_top']")
    @CacheLookup
    WebElement ccy_USDChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul/li[3]/a/label/input[@value='GBP_top']")
    @CacheLookup
    WebElement ccy_GBPChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/button")
    @CacheLookup
    WebElement dealer_ChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement dealer_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[2]/div/ul/li[2]/a/label/input[@value='Etrading Software Bank A']")
    @CacheLookup
    WebElement dealer_ETChkBox;


    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/button")
    @CacheLookup
    WebElement seniority_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement seniority_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[2]/div/ul/li[11]/a/label/input")
    @CacheLookup
    WebElement seniority_SenChkBox;


    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/button")
    @CacheLookup
    WebElement sectorDropdown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[1]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement sector_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[1]/div/ul/li[4]/a/label/input[@value='Banks']")
    @CacheLookup
    WebElement sector_Banks;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/button")
    @CacheLookup
    WebElement region_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[1]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement region_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[2]/div/ul/li[3]/a/label/input[@value='Asia']")
    @CacheLookup
    WebElement region_Asia;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/button")
    @CacheLookup
    WebElement ctryIssuer_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[1]/div/input[@placeholder='Search']")
    @CacheLookup
    WebElement ctryIssuer_txtField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[2]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement ctryIssuer_ALL;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[7]/div/div[2]/div/ul/li[197]/a/label/input[@value='United States']")
    @CacheLookup
    WebElement ctryIssuer_USA;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/button")
    @CacheLookup
    WebElement ctryRisk_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[2]/a/label/input[@value='ALL']")
    @CacheLookup
    WebElement ctryRisk_ALLChkBox;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[1]/div/input[@placeholder='Search']")
    @CacheLookup
    WebElement ctryRisk_textField;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[8]/div/div[1]/div/ul/li[196]/a/label/input[@value='United Kingdom']")
    @CacheLookup
    WebElement ctryRisk_GBR;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div/div[1]/input[@value='Asset Class (ALL)']")
    @CacheLookup
    WebElement assetClass_DropDown;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div[1]/div/ul/li[1]/span/input")
    @CacheLookup
    WebElement assetClass_ALL;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[11]/div/div[1]/div[1]/div/ul/li[6]/span/input[@data-value='Financial']")
    @CacheLookup
    WebElement assetClass_Financial;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input[@name='sec_id']")
    @CacheLookup
    WebElement grid_SecID;

    @FindBy(xpath = "//button[@class='btn btn-warning search-form-clearSearch']")
    @CacheLookup
    WebElement clearBtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[1]/button")
    @CacheLookup
    WebElement saveQuery;

    public AdvancedSearch(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        //Initialize advanced search page object
        PageFactory.initElements(driver, this);

    }

    public void enterSecurityID(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(securityIDField));
        securityIDField.sendKeys(value);
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        System.out.println("Entered ISIN:" + value);
    }

    public void keyPress() throws AWTException {
        //wait.until(ExpectedConditions.visibilityOf(securityIDField));
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", securityIDField);

        Robot robot = new Robot();

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        waitforPageLoaded();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

        wait.until(ExpectedConditions.visibilityOf(grid_SecID));
        if (grid_SecID.isDisplayed()) {
            Assert.assertTrue("Search thru Key Press Enter is Complete", true);
            System.out.println("Search thru Key Press Enter is Complete");
        } else {
            Assert.fail("Data not Found!");
            System.out.println("Data not Found!");
        }

    }

    public void clearSearchResults() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", clearBtn);
        System.out.println("Triggered clear button");
        //clearBtn.click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        //waitforPageLoaded();
        try {
            if(saveQuery.isDisplayed()) {
                //wait.until(ExpectedConditions.invisibilityOf(grid_SecID));
                Assert.assertTrue("Clear search results COMPLETED", true);
                System.out.println("Clear search results has been COMPLETED");
            }
        }catch(Exception e) {
            Assert.fail("Data was not cleared!");
            System.out.println("Data not cleared!");
        }
    }

    public void loopEnterSecID(String[] values, WebElement enterSecID) {
        for (String code : values) {
            enterSecID.sendKeys(code + "\n");
        }
    }

    public void enterSecurityName(String value) throws InterruptedException {

        //wait.until(ExpectedConditions.elementToBeClickable(securityNameField));
        securityNameField.sendKeys(value);
        Thread.sleep(6000);
        System.out.println("Entered Security Name is:" + value);

    }

    public void enterEntityName(String value) throws InterruptedException {
        entityNameField.sendKeys(value);
        Thread.sleep(6000);
        System.out.println("Entered Entity Name is:" + value);
    }

    public void enterMinandMax(String minValue, String maxValue) {

        minIDField.sendKeys(minValue);
        System.out.println("Entered min value is: " + minValue);

        maxIDField.sendKeys(maxValue);
        System.out.println("Entered max value is: " + maxValue);

    }


    public void enterMinValue(String value) {
        wait.until(ExpectedConditions.elementToBeClickable(minIDField));
        minIDField.sendKeys(value);
        System.out.println("Entered min value is: " + value);
    }

    public void enterMaxValue(String value) {

        wait.until(ExpectedConditions.elementToBeClickable(maxIDField));
        maxIDField.sendKeys(value);
        System.out.println("Entered max value is: " + value);

    }

    public void checkSingleCCY() {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        //((JavascriptExecutor) driver).executeScript("arguments[0].focus();", ccyDropdown);
        try {
            if(ccyDropdown.isDisplayed()){
                js.executeScript("arguments[0].click;", ccyDropdown);
                ccyDropdown.click();

                //Uncheck CCY - ALL
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                js.executeScript("arguments[0].click;", ccy_AllChkBox);
                ccy_AllChkBox.click();
                System.out.println("Triggered all checkbox");

                //Check USD
                wait.until(ExpectedConditions.elementToBeClickable(ccy_EURChkBox));
                js.executeScript("arguments[0].click;", ccy_EURChkBox);
                ccy_EURChkBox.click();
                System.out.println("Triggered eur checkbox");

            }
        }catch(Exception e){

            e.printStackTrace();
            System.out.println("Can't check any values from CCY dropdown list!");

        }
    }

    public void checkSingleDealer() {
        wait.until(ExpectedConditions.elementToBeClickable(dealer_ChkBox));
        js.executeScript("arguments[0].click;", dealer_ChkBox);
        dealer_ChkBox.click();

        //Uncheck - ALL
        js.executeScript("arguments[0].click;", dealer_ALLChkBox);
        dealer_ALLChkBox.click();
        System.out.println("Triggered dealer all checkbox");

        //check e trading
        js.executeScript("arguments[0].click;", dealer_ETChkBox);
        dealer_ETChkBox.click();
        System.out.println("Triggered dealer et checkbox");

    }

    public void checkSingleAssetClass() {

        wait.until(ExpectedConditions.visibilityOf(assetClass_DropDown));
        js.executeScript("arguments[0].click;", assetClass_DropDown);
        assetClass_DropDown.click();

        //Uncheck - ALL
        js.executeScript("arguments[0].click;", assetClass_ALL);
        assetClass_ALL.click();
        System.out.println("Triggered asset class all checkbox");

        //check asset class - financial
        js.executeScript("arguments[0].click;", assetClass_Financial);
        assetClass_Financial.click();
        System.out.println("Triggered asset class Financial checkbox");

    }

    public void checkSingleSector() {
        wait.until(ExpectedConditions.visibilityOf(sectorDropdown));
        js.executeScript("arguments[0].click;", sectorDropdown);
        sectorDropdown.click();

        //Uncheck - ALL
        js.executeScript("arguments[0].click;", sector_ALLChkBox);
        sector_ALLChkBox.click();
        System.out.println("Triggered sector all checkbox");

        //check e trading

        js.executeScript("arguments[0].click;", sector_Banks);
        sector_Banks.click();
        System.out.println("Triggered sector banks checkbox");

    }

    public void checkSingleRegion() {

        wait.until(ExpectedConditions.elementToBeClickable(region_DropDown));
        js.executeScript("arguments[0].click;", region_DropDown);
        region_DropDown.click();

        //uncheck all
        wait.until(ExpectedConditions.visibilityOf(region_ALLChkBox));
        js.executeScript("arguments[0].click;", region_ALLChkBox);
        region_ALLChkBox.click();
        System.out.println("Triggered region all checkbox");

        //Check asia from region
        wait.until(ExpectedConditions.visibilityOf(region_Asia));
        js.executeScript("arguments[0].click;", region_Asia);
        region_Asia.click();
        System.out.println("Triggered region asia checkbox");

    }

    public void checkSingleCtryofRisk(String country_code) {

        wait.until(ExpectedConditions.visibilityOf(ctryRisk_DropDown));
        js.executeScript("arguments[0].click;", ctryRisk_DropDown);
        ctryRisk_DropDown.click();

        //uncheck all
        js.executeScript("arguments[0].click;", ctryRisk_ALLChkBox);
        ctryRisk_ALLChkBox.click();
        System.out.println("Triggered ctry of issuer all checkbox");

        //enter specific country
        wait.until(ExpectedConditions.elementToBeClickable(ctryRisk_textField));
        ctryRisk_textField.sendKeys(country_code);

        //Check country from list
        js.executeScript("arguments[0].click;", ctryRisk_GBR);
        ctryRisk_GBR.click();
        System.out.println("Triggered ctry of issuer USA checkbox");

    }

    public void checkSingleCtryofIssuer(String country_code) {
        wait.until(ExpectedConditions.visibilityOf(ctryIssuer_DropDown));
        js.executeScript("arguments[0].click;", ctryIssuer_DropDown);
        ctryIssuer_DropDown.click();

        //uncheck all
        js.executeScript("arguments[0].click;", ctryIssuer_ALL);
        ctryIssuer_ALL.click();
        System.out.println("Triggered ctry of issuer all checkbox");

        //enter specific country
        wait.until(ExpectedConditions.elementToBeClickable(ctryIssuer_txtField));
        ctryIssuer_txtField.sendKeys(country_code);

        //Check country from list
        js.executeScript("arguments[0].click;", ctryIssuer_USA);
        ctryIssuer_USA.click();
        System.out.println("Triggered ctry of issuer USA checkbox");


    }

    public void checkSingleSeniority() {

        wait.until(ExpectedConditions.visibilityOf(seniority_DropDown));
        js.executeScript("arguments[0].click;", seniority_DropDown);
        seniority_DropDown.click();

        //uncheck all
        wait.until(ExpectedConditions.visibilityOf(seniority_ALLChkBox));
        js.executeScript("arguments[0].click;", seniority_ALLChkBox);
        seniority_ALLChkBox.click();
        System.out.println("Triggered seniority all checkbox");

        //Check country from list
        wait.until(ExpectedConditions.visibilityOf(seniority_SenChkBox));
        js.executeScript("arguments[0].click;", seniority_SenChkBox);
        seniority_SenChkBox.click();
        System.out.println("Triggered seniority Seniority checkbox");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);


        //repeat click
        wait.until(ExpectedConditions.visibilityOf(seniority_DropDown));
        js.executeScript("arguments[0].click;", seniority_DropDown);
        seniority_DropDown.click();

    }


    public void checkCCYfromList() {

        wait.until(ExpectedConditions.elementToBeClickable(ccyDropdown));
        ;
        js.executeScript("arguments[0].click;", ccyDropdown);
        ccyDropdown.click();
        //Uncheck CCY - ALL
        js.executeScript("arguments[0].click;", ccy_AllChkBox);
        ccy_AllChkBox.click();
        System.out.println("Triggered all checkbox");


        //check usd and gbp
        js.executeScript("arguments[0].click;", ccy_USDChkBox);
        ccy_USDChkBox.click();
        System.out.println("Triggered usd checkbox");

        js.executeScript("arguments[0].click;", ccy_GBPChkBox);
        ccy_GBPChkBox.click();
        System.out.println("Triggered gbp checkbox");

    }


    public void selectVALUEfromSIDE(String value) throws InterruptedException {
        WebElement select = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select"));
        System.out.println("Triggered select");

        Thread.sleep(6000);

        List<WebElement> options = select.findElements(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[4]/div/div[1]/select/option"));
        for (WebElement option : options) {
            if (value.equals(option.getText()))
                option.click();
            System.out.println("Selected option is: " + value);
        }

    }

    public void clickSearchBtn() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(searchBtn));
        try {
            if(searchBtn.isDisplayed()){
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", searchBtn);
                //searchBtn.click();
                System.out.println("Triggered search button");
                driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Element not clickable at this point!");

        }

    }

    public boolean retryingFindClick(By by) {
        boolean result = false;
        int attempts = 0;
        while (attempts < 2) {
            try {
                driver.findElement(by).click();
                result = true;
                break;
            } catch (StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
    }
}
