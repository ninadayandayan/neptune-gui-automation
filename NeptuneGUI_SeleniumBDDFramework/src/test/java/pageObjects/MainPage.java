package pageObjects;

import Base.BaseUtil;
import helper.Screenshots;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainPage extends BaseUtil {

    public JavascriptExecutor js = (JavascriptExecutor) driver;
    public Actions action = new Actions(driver);

    //Main Menus:
    @FindBy(xpath = "")
    @CacheLookup
    WebElement watchListTab;

    @FindBy(xpath = "//div[@class='slick-cell l1 r1']/span")
    @CacheLookup
    WebElement gridResult_SecID;

    @FindBy(xpath = "//div[@class='slick-cell l3 r3']/span")
    @CacheLookup
    WebElement gridResult_SecName;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'Deutsche')]")
    @CacheLookup
    WebElement gridResult_Entity;

    @FindBy(xpath = "//div[@class='slick-cell l9 r9']/span")
    @CacheLookup
    WebElement gridResult_SIDE;

    @FindBy(xpath = "//div[@class='slick-cell l15 r15 right-aligned']")
    @CacheLookup
    WebElement gridResult_Yield;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'EUR')]")
    @CacheLookup
    WebElement gridResult_CCY;

    @FindBy(xpath = "//*[contains(@id,'Entity Name')]//*[contains(text(),'Entity Name')]")
    @CacheLookup
    WebElement entityName;

    @FindBy(xpath = "//div[@class='slick-cell l13 r13']")
    WebElement gridResult_CCY_IE;

    @FindBy(xpath = "//strong[contains(text(),'Etrading')]")
    @CacheLookup
    WebElement gridResult_Dealer;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'Banks')]")
    @CacheLookup
    WebElement gridResult_Sector;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'Asia')]")
    @CacheLookup
    WebElement gridResult_Region;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'United States')]")
    @CacheLookup
    WebElement gridResult_CtryIssuer;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'United Kingdom')]")
    @CacheLookup
    WebElement gridResult_CtryRisk;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'Financial')]")
    @CacheLookup
    WebElement gridResult_AssetClass;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]//*[contains(text(),'Senior')]")
    @CacheLookup
    WebElement gridResult_Seniority;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div/div[1]/div[13]")
    @CacheLookup
    WebElement gridResult_Status;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[6]/button[@title='Fullscreen Table']")
    @CacheLookup
    WebElement fullScreenbtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[1]/div[2]/span/ul/li[6]/button[1]")
    @CacheLookup
    WebElement expandTablebtn;

    @FindBy(xpath = "//button[@class='btn btn-link dropdown-toggle recent-searches']")
    @CacheLookup
    WebElement recentSearchBtn;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[1]/div/div[1]/input")
    @CacheLookup
    WebElement securityIDField;

    @FindBy(xpath = "//div[contains(@id,'Yield')]/div[1]")
    @CacheLookup
    WebElement sortYield;

    @FindBy(xpath = "//*[@id='dashboard-tab']/div/div[2]/div[2]/div/section/div/div/div/ul/li[4]/span[@class='info']")
    @CacheLookup
    WebElement marketDepth_info;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 60);
        //initialize page object
        PageFactory.initElements(driver, this);
    }

    public void clickFUllScreen() throws InterruptedException {
        js.executeScript("arguments[0].click();", fullScreenbtn);
        fullScreenbtn.click();
        Thread.sleep(9000);
        System.out.println("Triggered Full Screen View");
    }

    public void clickExpand() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(expandTablebtn));
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", expandTablebtn);
        js.executeScript("arguments[0].click;", expandTablebtn);
        //expandTablebtn.click();
        System.out.println("Triggered expand table");
    }

    public void clickYield() {
        js.executeScript("arguments[0].click;", sortYield);
        sortYield.click();
        System.out.println("Triggered yield sort");
    }

    public void selectIndication() {
        wait.until(ExpectedConditions.elementToBeClickable(gridResult_SecID));
        js.executeScript("arguments[0].click;", gridResult_SecID);
        gridResult_SecID.click();
        System.out.println("Selected indication from the search result");

        //Validate market depth tab
        wait.until(ExpectedConditions.visibilityOf(marketDepth_info));
        if (marketDepth_info.isDisplayed()) {
            Assert.assertTrue("Market Depth has been loaded", true);
            System.out.println("Market Depth has been loaded");
        } else {
            Assert.fail("Market Depth DID not LOAD!");
            System.out.println("Market Depth DID not LOAD!");
        }

    }

    public void clickExpandScreen() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.visibilityOf(expandTablebtn));
        //click fit to screen
        expandTablebtn.click();
        wait.until(ExpectedConditions.visibilityOf(gridResult_Status));
        if (gridResult_Status.isDisplayed()) {
            Assert.assertTrue("Expand Screen is now ENABLED", true);
            System.out.println("Expand Screen is now ENABLED");

        } else {
            Assert.fail("Expand screen DID not triggered!");
        }

    }

    public void clickFitToScreen() throws InterruptedException {
        //click fit to screen
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.elementToBeClickable(fullScreenbtn));
        //js.executeScript("arguments[0].click;", fullScreenbtn);
        fullScreenbtn.click();
        System.out.println("Triggered full screen button");
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        //wait.until(ExpectedConditions.visibilityOf(gridResult_Status));

        try {
            if (driver.findElements(By.xpath("//*[contains(@id,'Entity Name')]//*[contains(text(),'Entity Name')]")).isEmpty()) {
                Assert.assertTrue("Full Screen is now ENABLED", true);
                System.out.println("Full Screen is now ENABLED");
            }
        }catch (Exception e){

            Assert.fail("FULL screen did not triggered!");
            e.printStackTrace();

        }

    }


    public void selectValuefropDropdownList(String value) {
        WebElement select = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[1]/div[1]/div/form[2]/div[2]/div/div[1]/div[3]/div/div[1]/div/ul"));
        List<WebElement> options = select.findElements(By.tagName("li"));
        for (WebElement option : options) {
            if (value.equals(option.getText()))
                option.click();
        }
    }

    public void scrollScreen() {

    }

    public void checkLoop() {

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 31; i++) {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
            }

        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        System.out.println("Key press complete!");

    }


    public void checkMidLoop() {

        try {
            Robot robot = new Robot();

            for (int i = 0; i <= 25; i++) {
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
            }

        } catch (AWTException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        System.out.println("Key press complete!");

    }



    //Verification Part:

    public void verifyGrid_EntityName(String value) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", gridResult_Entity);
        wait.until(ExpectedConditions.visibilityOf(gridResult_Entity));
        System.out.println("Entity Name is visible");

        try {

            if (!gridResult_Entity.getText().equals(value)) {
                wait.until(ExpectedConditions.visibilityOf(gridResult_Entity));

                System.out.println("Entity Name is visible again");
                Assert.assertTrue("Search Complete", true);
                System.out.println("Search Complete" + "Value displayed is: " + value);
            }
        }catch (Exception e){

            Assert.fail("Data not found");
            System.out.println("Data not found!");
            e.printStackTrace();

        }

    }

    public void verifyGrid_SIDE(String value) {

        if (gridResult_SIDE.getText().equals(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }
    }

    public void verifyGrid_Min(String value) {

        if (gridResult_Yield.getText().contains(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }

    }

    public void verifySortYield() {
        String temp = null;
        /**
         * Retrieve the List of Items in the Table before Sorting and Store into Array
         */
        List<WebElement> tdList = driver.findElements(By.xpath("//div[@class='slick-cell l15 r15 right-aligned']"));
        String strArray[] = new String[tdList.size()];
        for (int i = 0; i < tdList.size(); i++) {
            System.out.println(tdList.get(i).getText());
            strArray[i] = tdList.get(i).getText();
        }

        /**
         * Sort the Array by Swapping the Elements
         */
        for (int i = 0; i < strArray.length; i++) {
            for (int j = i + 1; j < strArray.length; j++) {
                if (strArray[i].compareTo(strArray[j]) > 0) {
                    temp = strArray[i];
                    strArray[i] = strArray[j];
                    strArray[j] = temp;
                }
            }
        }

        /**
         * Printing the Values after sorting
         */
        System.out.println("##################Sorted values in the Array####################");
        for (int i = 0; i < strArray.length; i++) {

            System.out.println(strArray[i]);
        }


    }

    public void verifyGrid_secName(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_SecName));
        if (gridResult_SecName.getText().contains(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_PartialSecID(String partial_value) {

        //get main grid
        WebElement main_grid = driver.findElement(By.xpath("//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div"));
        //get rows:
        By gridRows = By.xpath("//*[@id='dashboard-tab']/div/div[2]/div[1]/div/section/div/div/div[1]/div/div/div[2]/div/div[2]/div/div/div[5]/div//*[contains(@class,'ui-widget-content slick-row')]");
        List<WebElement> allRows = main_grid.findElements(gridRows);
        //loop through each row and compare actual value with expected value no.
        for (WebElement row : allRows) {
            //Get the column
            By isinColumn = By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[1]");

            String actualValue = row.findElement(isinColumn).getText();
            //Compare actual vs. expected
            if (actualValue.equals(partial_value)) {
                row.click();
                System.out.println("Selected row " + (allRows.indexOf(row) + 1) + " having Actual. No. " + partial_value);
                break;
            }
        }
    }

    public void testColumnAgain() {
        List<WebElement> isin_Column = driver.findElements(By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[1]"));
        System.out.println("NUMBER OF ROWS IN THIS TABLE = " + isin_Column.size());
        int row_num = 0, col_num = 0;
        for (WebElement trElement : isin_Column) {
            List<WebElement> td_collection = trElement.findElements(By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span"));
            //List<WebElement> td_collection = trElement.findElements(By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span"));
            int n = td_collection.size();
            System.out.println(td_collection.size());
            for (int i = 0; i < td_collection.size(); i++)
                System.out.println(td_collection.get(i).getText());
            System.out.println("NUMBER OF COLUMNS=" + td_collection.size());

            for (WebElement tdElement : isin_Column) {
                System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
                col_num++;
            }
            row_num++;
        }

    }

    public void testColumn() {
        List<WebElement> isin_Column = driver.findElements(By.xpath("//*[contains(@title,'Add to Watch List')]/following-sibling::div[1]"));
        System.out.println("NUMBER OF ROWS IN THIS COLUMN = " + isin_Column.size());
        int row_num = 1, col_num = 1;
        for (WebElement trElement : isin_Column) {
            List<WebElement> td_collection = trElement.findElements(By.xpath("//*[contains(@class,'ui-widget-content slick-row')]//*[contains(@class,'slick-cell l1 r1')]/span"));
            int n = td_collection.size();
            for (int i = 0; i < td_collection.size(); i++)
                System.out.println(td_collection.get(i).getText());
            System.out.println("NUMBER OF COLUMNS=" + td_collection.size());
            col_num = 1;
            for (WebElement tdElement : isin_Column) {
                System.out.println("row # " + row_num + ", col # " + col_num + "text=" + tdElement.getText());
                row_num++;
            }
        }
    }

    public void verifyGrid_secID(String value) throws InterruptedException, IOException {
        wait.until(ExpectedConditions.visibilityOf(gridResult_SecID));
        if (gridResult_SecID.getText().contains(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            Screenshots.takeScreenshot(driver);
            System.out.println("Data not found!");
        }
    }

    public void verifyGrid_CCY(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_CCY));
        if (gridResult_CCY.getText().equals(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found");
            System.out.println("Data not found!");
        }

    }

    public void strongclass() {

    }

    public void getTextfromStrongClass() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Object load = js.executeScript("var value = document.evaluate(\"//div[@class='slick-cell l7 r7']/strong\",document, null, XPathResult.STRING_TYPE, null ); return value.stringValue;");
        System.out.println("Load Number : " + load.toString());

    }

    public void verifyGrid_Dealer(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Dealer));

        if (gridResult_Dealer.getText().equals(value)) {
            Assert.assertTrue("Search Complete", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");

        }

    }

    public void verifyGrid_Sector(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Sector));

        if (gridResult_Sector.getText().contains(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }
    }

    public void verifyGrid_Region(String value) {
        wait.until(ExpectedConditions.visibilityOf(gridResult_Region));

        if (gridResult_Region.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_CtryIssuer(String value) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].focus();", gridResult_CtryIssuer);
        wait.until(ExpectedConditions.visibilityOf(gridResult_CtryIssuer));

        try {
            if (!gridResult_CtryIssuer.getText().contains(value)) {
                wait.until(ExpectedConditions.visibilityOf(gridResult_CtryIssuer));
                Assert.assertTrue("Search Complete ", true);
                System.out.println("Search Complete" + "Value displayed is: " + value);
            }
        }catch(Exception e){
                Assert.fail("Data not found!");
                System.out.println("Data not found!");
                e.printStackTrace();
        }
    }

    public void verifyGrid_CtryRisk(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_CtryRisk));

        if (gridResult_CtryRisk.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_AssetClass(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_AssetClass));

        if (gridResult_AssetClass.getText().contains(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }

    }

    public void verifyGrid_Seniority(String value) {

        wait.until(ExpectedConditions.visibilityOf(gridResult_Seniority));

        if (gridResult_Seniority.getText().equals(value)) {
            Assert.assertTrue("Search Complete ", true);
            System.out.println("Search Complete" + "Value displayed is: " + value);
        } else {
            Assert.fail("Data not found!");
            System.out.println("Data not found!");
        }
    }

    public void clickRecentSearchandVerify(String query) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(recentSearchBtn));

        try {
            js.executeScript("arguments[0].click();", recentSearchBtn);
            recentSearchBtn.click();

            System.out.println("Triggered recent search button");
            //get query:
            String result = driver.findElement(By.xpath("//ul/li/a[@class='recent-search-link']")).getText();
            if (!result.equals(query)) {
                System.out.println("Query is found in Recent search");
                js.executeScript("arguments[0].click();", recentSearchBtn);
                recentSearchBtn.click();
            } else {
                Assert.fail("Query not FOUND in Recent Search!");
                System.out.println("Query not FOUND in Recent search!");
            }

        }catch(Exception e){

            e.printStackTrace();

        }

    }
}
