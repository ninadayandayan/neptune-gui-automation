Feature: Advanced Search: Enter a search with partial Security ID

  @Functional
  Scenario Outline:Enter a search with partial Security ID
    When User clicks on Advanced Search tab
    And enter single partial <value> search on Security ID field
    Then verify if search parameter <value> is displayed in Dashboard - Partial Security ID
    Then verify if search parameter is displayed in Recent Search as <query>
    And User sign out successfully

    Examples:
      | value        |query       |
      | DE           |DE          |







