package Archive;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.MainPage;

public class TC143_Steps extends BaseUtil {

    //Initialize page object:
    MainPage main = new MainPage(driver);

    @Then("verify if search parameter is displayed in Recent Search Label as {}")
    public void verify_if_search_parameter_is_displayed_in_Recent_Search_Label(String query) throws InterruptedException {

        main.clickRecentSearchandVerify(query);

    }


}
