package Archive;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

import java.io.IOException;

public class TC85_Steps extends BaseUtil {

    //Initialize page object
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);


    @When("enter single partial DE search on Security ID field")
    public void enter_single_partial_DE_search_on_Security_ID_field(String value) throws InterruptedException {
        advancedSearch.enterSecurityID(value);
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @Then("verify if search parameter DE is displayed in Dashboard - Partial Security ID")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Partial_Security_ID(String value) throws IOException, InterruptedException {

        main.verifyGrid_secID(value);
    }







    @When("enter single partial {} search on Security ID field")
    public void enter_single_partial_search_on_Security_ID_field(String value) throws InterruptedException {
        advancedSearch.enterSecurityID(value);
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @Then("verify if search parameter {} is displayed in Dashboard - Partial Security ID")
    public void verify_if_search_parameter_DE_is_displayed_in_Dashboard_Partial_Security_ID(String value) throws IOException, InterruptedException {

        main.verifyGrid_secID(value);
    }

    @When("enter single partial {} search on Security ID field")
    public void enter_single_partial_DE_search_on_Security_ID_field() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then("verify if search parameter {} is displayed in Dashboard - Partial Security ID")
    public void verify_if_search_parameter_DE_is_displayed_in_Dashboard_Partial_Security_ID() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }





}
