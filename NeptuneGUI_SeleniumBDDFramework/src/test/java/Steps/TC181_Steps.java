package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC181_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch  = new AdvancedSearch(driver);


    @When("user selects single region  on region drop down list")
    public void enter_selects_single_region_on_region_drop_down_list() throws InterruptedException {


        advancedSearch.checkSingleRegion();
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @Then("verify if search parameter {} is displayed in Dashboard - Region")
    public void verify_if_search_parameter_Asia_is_displayed_in_Dashboard_Region(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Region(value);

    }


}
