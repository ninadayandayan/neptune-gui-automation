package Steps;

import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class Hook extends BaseUtil implements Screenshots {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String urlDEV,urlPROD, urlCERT,environment, browser;
    private BaseUtil base;

    public Hook(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void initializeTest(Scenario scenario) throws IOException {
        properties.load(new FileInputStream(testData));
        browser = properties.getProperty("browserName");

        System.out.println("---Initialize Test---");
        System.out.println("Started execution for the scenario: " + scenario.getName());

        switch (browser){

            case "chrome":
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//src/Drivers/chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--ignore-certificate-errors");
                options.addArguments("--start-maximized");
                driver = new ChromeDriver(options);
                driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
                System.out.println("Driver used is:" + driver);
                break;
            case "ie":
                System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")  + "//src/Drivers/IEDriverServer.exe");
                DesiredCapabilities ieCapabilities = new DesiredCapabilities();
                ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                ieCapabilities.setCapability("nativeEvents", false);
                ieCapabilities.setCapability("unexpectedAlertBehaviour", "accept");
                ieCapabilities.setCapability("ignoreProtectedModeSettings", true);
                ieCapabilities.setCapability("disable-popup-blocking", true);
                ieCapabilities.setCapability("enablePersistentHover", true);
                ieCapabilities.setCapability("ignoreZoomSetting", true);
                driver = new InternetExplorerDriver(ieCapabilities);
                driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
                break;

            case "ff":
                System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "//src/Drivers/geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
                break;

            default:
                System.out.println(browser + " is invalid, Launching Chrome as default browser of choice..");
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "neptune-gui-automation//NeptuneGUI_SeleniumBDDFramework//src/Drivers/chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        }
        //get environment details:
        System.out.println("---Get Environment Details---");
        environment = properties.getProperty("environmentName");
        switch (environment) {
            case "DEV":

                urlDEV = properties.getProperty("url_DEV");
                driver.get(urlDEV);
                driver.navigate ().to ("javascript:document.getElementById('overridelink').click()");
                System.out.println("Accessing Environment: " + urlDEV);
                break;

            case "PROD":
                urlPROD = properties.getProperty("url_PROD");
                driver.get(urlPROD);
                System.out.println("Accessing Environment: " + urlPROD);
                break;
            //System.out.println("Enviroment Name is: " +environment);

            case "CERT":
                urlCERT = properties.getProperty("url_CERT");
                driver.get(urlCERT);
                System.out.println("Accessing Environment: " + urlCERT);
                break;
            //System.out.println("Enviroment Name is: " +environment);

            default:
                System.out.println("Environment Name : " + environment + " is invalid, Launching DEV as environment name of choice..");
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "neptune-gui-automation//NeptuneGUI_SeleniumBDDFramework//src/Drivers/chromedriver.exe");
                urlDEV = properties.getProperty("url_DEV");
                driver.get(urlDEV);
                System.out.println("Accessing Environment: " + urlDEV);
        }
    }
    @After
    public void tearDownTest(Scenario scenario) throws IOException {
        System.out.println("Completed execution for the scenario: " + scenario.getName());
        if(scenario.isFailed()){
            Screenshots.takeScreenshot(driver);
        }
        System.out.println("---Closing browser---");
        driver.quit();
    }
}
