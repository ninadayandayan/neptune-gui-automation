package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC168_Steps extends BaseUtil {

    //Initialize page object
    AdvancedSearch advancedsearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);

    @When("enter parameter {} search on Entity Name field")
    public void enter_parameter_search_on_Entity_Name_field(String value) throws InterruptedException {

        advancedsearch.enterEntityName(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Entity Name")
    public void verify_if_search_parameter_entity_name_is_displayed_in_Dashboard_Entity_Name(String value) throws InterruptedException {

        main.clickExpand();
        main.checkMidLoop();
        main.verifyGrid_EntityName(value);

    }


}
