package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.MainPage;

import java.io.IOException;

public class TC232_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);

    @Given("User has successfully Login")
    public void user_has_successfully_Login() throws IOException, InterruptedException {

        //loginToApp();
    }

    @Then("User clicks on expand button")
    public void user_clicks_expand_button() throws InterruptedException {

        main.clickExpandScreen();
        waitforPageLoaded();

    }
}
