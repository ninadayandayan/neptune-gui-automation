package Steps;
import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.LoginPage;
import pageObjects.MainPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TC165_Steps extends BaseUtil {

    //Initialize page object
    AdvancedSearch advancedsearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);

    @When("enter ticker {} search on Security Name field")
    public void enter_ticker_value_search_on_Security_Name_field(String value) throws InterruptedException {

        advancedsearch.enterSecurityName(value);
        advancedsearch.clickSearchBtn();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Security Name")
    public void verifyIfSearchParameterValueIsDisplayedInDashboardSecurityName(String value) {

        main.verifyGrid_secName(value);
    }
}
