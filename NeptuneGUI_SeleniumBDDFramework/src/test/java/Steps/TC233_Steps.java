package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.Then;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC233_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);

    @Then("User clicks on fit to screen button")
    public void user_clicks_fit_to_screen_button() throws InterruptedException {

        main.clickFitToScreen();
        waitforPageLoaded();

    }


}
