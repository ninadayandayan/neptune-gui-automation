package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC171_Steps extends BaseUtil {


    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch  = new AdvancedSearch(driver);


    @When("user selects single sector  on sector drop down list")
    public void enter_selects_single_sector_on_sector_drop_down_list() throws InterruptedException {

        advancedSearch.checkSingleSector();
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Sector")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Sector(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Sector(value);
    }
}
