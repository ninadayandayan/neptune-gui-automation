package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.LoginPage;

import java.awt.*;

public class TC84_Steps extends BaseUtil {

    //Initialize page objects:
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);

    @Then("User press enter from keyboard")
    public void user_press_enter_from_keyboard() throws AWTException {

        advancedSearch.keyPress();
        waitforPageLoaded();

    }
}
