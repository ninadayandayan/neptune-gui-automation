package Steps;

import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.LoginPage;
import pageObjects.MainPage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Main extends BaseUtil {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String companyID,username,password;

    //Initialize page object class
    LoginPage login = new LoginPage(driver);
    AdvancedSearch advancedsearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);

    @Before
    public void loginToApp() throws IOException, InterruptedException, AWTException {

        properties.load(new FileInputStream(testData));
        companyID = properties.getProperty("companyID");
        username = properties.getProperty("username");
        password  = properties.getProperty("password");

        login.setCompanyID(companyID);
        login.setUsername(username);
        login.setPassword(password);
        Thread.sleep(6000);
        waitforPageLoaded();
    }

    @And("User sign out successfully")
    public void userSignOutSuccessfully() throws IOException, InterruptedException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        login.selectFromMainMenu();
        login.clickLogout();

    }

    @Then("User clicks on fit to screen button")
    public void user_clicks_fit_to_screen_button() throws InterruptedException {

        main.clickFitToScreen();
        waitforPageLoaded();

    }

    @Then("User clicks on expand button")
    public void user_clicks_expand_button() throws InterruptedException {

        main.clickExpandScreen();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Security ID")
    public void verifyIfSearchParameterIsDisplayedInDashboard(String value) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        main.verifyGrid_secID(value);


    }

    @Then("verify if search parameter is displayed in Recent Search as {}")
    public void verifyIfSearchParameterIsDisplayedInRecentSearchAs(String query) throws InterruptedException, IOException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        main.clickRecentSearchandVerify(query);

    }

    @Then("User should see the dashboard - {string} page")
    public void user_should_see_the_dashboard_page(String title) throws IOException {

        login.verifyTitle(title);
        Screenshots.takeScreenshot(driver);
        waitforPageLoaded();


    }

    @Given("User navigate to the login page")
    public void iNavigateToTheLoginPage() throws Throwable {

    }
    @And("User enters the login credentials")
    public void iEnterTheFollowingForLogin() throws Throwable {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

    }

    @And("User clicks the login button")
    public void iClickLoginButton() throws  Throwable{

    }

    @When("User select indication and validate market depth load")
    public void user_select_indication_and_validate_market_depth_load() {

        main.selectIndication();

    }

    @Then("verify if search parameter {} is displayed in Dashboard -  CCY")
    public void verify_if_search_parameter_Deutsche_Bahn_is_displayed_in_Dashboard_CCY(String value) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

        main.clickExpand();
        main.checkMidLoop();
        main.verifyGrid_CCY(value);

    }



}
