package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC214_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch  = new AdvancedSearch(driver);

    @When("user selects single asset class on Asset Class drop down list")
    public void user_selects_single_asset_class_on_Asset_Class_drop_down_list() throws InterruptedException {

        advancedSearch.checkSingleAssetClass();
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Asset Class")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Asset_Class(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_AssetClass(value);

    }
}
