package Steps;

import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageObjects.LoginPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TC01_Steps extends BaseUtil {


    //Initialize page object class
    LoginPage login = new LoginPage(driver);


    @Given("User navigate to the login page")
    public void iNavigateToTheLoginPage() throws Throwable {


    }
    @And("User enters the login credentials")
    public void iEnterTheFollowingForLogin() throws Throwable {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);


     /*  List<List<String>> data = table.asLists(String.class);
        System.out.println(data.get(1).get(0));
        System.out.println(data.get(1).get(1));*/
    //create an array list
   /*     List<User> users;
        //Store all users
        users = table.asList(User.class);

        for(User user: users){
            System.out.println("The username is: " + user.username);
            System.out.println("The password is: " + user.password);
        }*/
    }

    @And("User clicks the login button")
    public void iClickLoginButton() throws  Throwable{



    }


    @Then("User should see the dashboard - {string} page")
    public void user_should_see_the_dashboard_page(String title) throws IOException {

        login.verifyTitle(title);
        Screenshots.takeScreenshot(driver);
        waitforPageLoaded();


    }

 /*   public class User {

        public String username,password;

        public User(String userName,String passWord){
            username = userName;
            password = passWord;

        }
    }*/




}
