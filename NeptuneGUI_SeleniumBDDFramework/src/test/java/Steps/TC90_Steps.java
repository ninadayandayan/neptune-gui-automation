package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC90_Steps extends BaseUtil {

    //Initialize page object
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);

    @When("User select SIDE {} from SIDE drop down list")
    public void user_select_SIDE_BID_from_SIDE_drop_down_list(String value) throws InterruptedException {

        advancedSearch.selectVALUEfromSIDE(value);
        advancedSearch.clickSearchBtn();
        Thread.sleep(6000);
        waitforPageLoaded();

    }

    @Then("verify if side {} is displayed in Dashboard")
    public void verifyIfSideValueIsDisplayedInDashboard(String value) throws InterruptedException {

        main.clickFUllScreen();
        waitforPageLoaded();
        main.verifyGrid_SIDE(value);

    }
}
