package Steps;

import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.LoginPage;
import pageObjects.MainPage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Search extends BaseUtil implements Screenshots {

    public Properties properties = new Properties();
    public File testData = new File("src/main/resources/Configuration.properties");
    public String companyID,username,password;

    //Initialize page object class
    LoginPage login = new LoginPage(driver);
    AdvancedSearch advancedsearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);


    @When("User clicks on Advanced Search tab")
    public void user_clicks_on_Advanced_Search_tab() throws IOException {

        System.out.println("Default tab is Advanced Search");


    }

    @When("enter parameter {} search on Entity Name field")
    public void enter_parameter_search_on_Entity_Name_field(String value) throws InterruptedException {

        advancedsearch.enterEntityName(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Entity Name")
    public void verify_if_search_parameter_entity_name_is_displayed_in_Dashboard_Entity_Name(String value) throws InterruptedException {

        main.clickExpand();
        main.checkMidLoop();
        main.verifyGrid_EntityName(value);

    }

    @When("enter selects single ccy  on CCY drop down list")
    public void enter_selects_single_ccy_on_CCY_drop_down_list() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        advancedsearch.checkSingleCCY();
        waitforPageLoaded();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @And("enter single full {} search on Security ID field")
    public void enterSingleFullSearchOnSecurityIDField(String value) throws InterruptedException {

        advancedsearch.enterSecurityID(value);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }



    @Then("User press clear to clear search results")
    public void user_press_clear_to_clear_search_results() throws InterruptedException, AWTException {

        advancedsearch.keyPress();
        waitforPageLoaded();
        advancedsearch.clearSearchResults();

    }

    @When("enter ticker {} search on Security Name field")
    public void enter_ticker_value_search_on_Security_Name_field(String value) throws InterruptedException {

        advancedsearch.enterSecurityName(value);
        advancedsearch.clickSearchBtn();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Security Name")
    public void verifyIfSearchParameterValueIsDisplayedInDashboardSecurityName(String value) {

        main.verifyGrid_secName(value);
    }

    @When("user selects single sector  on sector drop down list")
    public void enter_selects_single_sector_on_sector_drop_down_list() throws InterruptedException {

        advancedsearch.checkSingleSector();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Sector")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Sector(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Sector(value);
    }

    @When("user selects single Country of Issuer {} on Ctry of Issuer drop down list")
    public void user_selects_single_Country_of_Issuer_on_Ctry_of_Issuer_drop_down_list(String country_code) throws InterruptedException {

        advancedsearch.checkSingleCtryofIssuer(country_code);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Ctry of Issuer")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Ctry_of_Issuer(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_CtryIssuer(value);
    }


    @When("user selects single Country of Risk {} on Ctry of Risk drop down list")
    public void user_selects_single_Country_of_Risk_on_Ctry_of_Risk_drop_down_list(String country_code) throws InterruptedException {

        advancedsearch.checkSingleCtryofRisk(country_code);
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Ctry of Risk")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Ctry_of_Risk(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_CtryRisk(value);
    }

    @When("user selects single region  on region drop down list")
    public void enter_selects_single_region_on_region_drop_down_list() throws InterruptedException {


        advancedsearch.checkSingleRegion();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();
    }

    @Then("verify if search parameter {} is displayed in Dashboard - Region")
    public void verify_if_search_parameter_Asia_is_displayed_in_Dashboard_Region(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Region(value);

    }

    @When("user selects single seniority on Seniority drop down list")
    public void user_selects_single_seniority_on_Seniority_drop_down_list() throws InterruptedException {

        advancedsearch.checkSingleSeniority();
        //advancedSearch.retryingFindClick(By.xpath("//button[@class='btn btn-primary search-form-performSearch']"));
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Seniority")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Seniority(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Seniority(value);

    }

    @When("user selects single asset class on Asset Class drop down list")
    public void user_selects_single_asset_class_on_Asset_Class_drop_down_list() throws InterruptedException {

        advancedsearch.checkSingleAssetClass();
        advancedsearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Asset Class")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Asset_Class(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_AssetClass(value);

    }

    @Then("User press enter from keyboard")
    public void user_press_enter_from_keyboard() throws AWTException {

        advancedsearch.keyPress();
        waitforPageLoaded();

    }










}
