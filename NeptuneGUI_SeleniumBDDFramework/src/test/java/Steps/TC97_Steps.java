package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.Then;
import pageObjects.AdvancedSearch;

import java.awt.*;

public class TC97_Steps extends BaseUtil {

    //Initialize page objects:
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);


    @Then("User press clear to clear search results")
    public void user_press_clear_to_clear_search_results() throws InterruptedException, AWTException {

        advancedSearch.keyPress();
        waitforPageLoaded();
        advancedSearch.clearSearchResults();

    }



}
