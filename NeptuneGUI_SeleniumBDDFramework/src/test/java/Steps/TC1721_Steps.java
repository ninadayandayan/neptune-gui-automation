package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC1721_Steps extends BaseUtil {

    //Initialize page objects
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);

    @When("enter selects single dealer  on dealer drop down list")
    public void enter_selects_single_dealer_on_dealer_drop_down_list() throws InterruptedException {

        advancedSearch.checkSingleDealer();
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Dealer")
    public void verify_if_search_parameter_dealer_is_displayed_in_Dashboard_dealer(String value) throws InterruptedException {

        //main.clickExpand();
        //main.checkLoop();
        //main.getTextfromStrongClass();
        main.verifyGrid_Dealer(value);

    }
}
