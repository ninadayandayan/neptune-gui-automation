package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.MainPage;

public class TC146_Steps extends BaseUtil {

    //Initialize page objects
    MainPage main = new MainPage(driver);

    @When("User select indication and validate market depth load")
    public void user_select_indication_and_validate_market_depth_load() {

    main.selectIndication();

    }
}
