package Steps;

import Base.BaseUtil;
import helper.Screenshots;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TC155_Steps extends BaseUtil {

    //Initialize page objects:
    AdvancedSearch advancedsearch = new AdvancedSearch(driver);
    MainPage main = new MainPage(driver);

    @When("enter min  {} search on Min field")
    public void enter_min_value_search_on_Min_field(String value) throws InterruptedException {

        advancedsearch.enterMinValue(value);
        advancedsearch.clickSearchBtn();

    }
    @Then("verify if search parameter {} is displayed in Dashboard - MIN")
    public void verify_if_search_parameter_value_is_displayed_in_Dashboard_MIN(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
       // main.clickYield();
        main.verifySortYield();
        main.verifyGrid_Min(value);

    }

    @Then("verify if search parameter is displayed in Recent Search - MIN as {}")
    public void verifyIfSearchParameterIsDisplayedInRecentSearchAs(String query) throws InterruptedException, IOException {

        main.clickRecentSearchandVerify(query);

    }

}
