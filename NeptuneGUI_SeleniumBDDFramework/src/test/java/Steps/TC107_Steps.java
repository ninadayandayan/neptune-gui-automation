package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

import java.util.concurrent.TimeUnit;

public class TC107_Steps extends BaseUtil {


    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch  = new AdvancedSearch(driver);

    @When("enter selects single ccy  on CCY drop down list")
    public void enter_selects_single_ccy_on_CCY_drop_down_list() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

            advancedSearch.checkSingleCCY();
            waitforPageLoaded();
            advancedSearch.clickSearchBtn();
            waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard -  CCY")
    public void verify_if_search_parameter_Deutsche_Bahn_is_displayed_in_Dashboard_CCY(String value) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

        main.clickExpand();
        main.checkMidLoop();
        main.verifyGrid_CCY(value);

    }


}
