package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC211_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch  = new AdvancedSearch(driver);

    @When("user selects single seniority on Seniority drop down list")
    public void user_selects_single_seniority_on_Seniority_drop_down_list() throws InterruptedException {

        advancedSearch.checkSingleSeniority();
        //advancedSearch.retryingFindClick(By.xpath("//button[@class='btn btn-primary search-form-performSearch']"));
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Seniority")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Seniority(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_Seniority(value);

    }
}
