package Steps;

import Base.BaseUtil;
import io.cucumber.java.en.*;
import pageObjects.AdvancedSearch;
import pageObjects.MainPage;

public class TC178_Steps extends BaseUtil {

    //Initialize page objects:
    MainPage main = new MainPage(driver);
    AdvancedSearch advancedSearch = new AdvancedSearch(driver);

    @When("user selects single Country of Risk {} on Ctry of Risk drop down list")
    public void user_selects_single_Country_of_Risk_on_Ctry_of_Risk_drop_down_list(String country_code) throws InterruptedException {

        advancedSearch.checkSingleCtryofRisk(country_code);
        advancedSearch.clickSearchBtn();
        waitforPageLoaded();

    }

    @Then("verify if search parameter {} is displayed in Dashboard - Ctry of Risk")
    public void verify_if_search_parameter_is_displayed_in_Dashboard_Ctry_of_Risk(String value) throws InterruptedException {

        main.clickExpand();
        main.checkLoop();
        main.verifyGrid_CtryRisk(value);


    }

}
