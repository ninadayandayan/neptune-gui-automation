package helper;

import com.aventstack.extentreports.utils.FileUtil;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface Screenshots {

    public static void takeScreenshot(WebDriver driver) throws IOException {

        //Convert web driver object to Takescreenshot
        TakesScreenshot screenshot = ((TakesScreenshot)(driver));

        //Call getScreenshot as method to create image file
        File scrFile = screenshot.getScreenshotAs(OutputType.FILE);

        // Open the current date and time
        String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());

        //move image file to new destination
        FileHandler.copy(scrFile,new File(System.getProperty("user.dir")+   "\\src\\Screenshots\\"+timestamp+ ".png"));
    }


}
