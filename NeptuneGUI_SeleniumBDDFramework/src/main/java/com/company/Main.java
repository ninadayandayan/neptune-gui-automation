package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Main {


    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello you're working from home");
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "//src/Drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://www.facebook.com/");
        driver.findElement(By.id("email")).sendKeys("your_username");
        driver.quit();
    }
}
